package main

import (
	"docker/trade"
	"log"
	"net/http"
)

func main() {
	tradeSys := trade.InitTrade()
	tradeSys.HandleWsApis()
	go tradeSys.StartReceiveAddTrade()
	log.Println("server start at :8899")
	log.Fatal(http.ListenAndServe(":8899", nil))
}

