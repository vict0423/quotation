package connect

import (
	"github.com/togettoyou/wsc"
)

type WsClient struct {
	Client *wsc.Wsc
	Done chan bool
}

func InitWebSocketClient(remote string ) *WsClient {
	done := make(chan bool)
	return &WsClient{
		Client: wsc.New(remote),
		Done:   done,
	}
}