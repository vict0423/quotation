package connect

import (
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

type WsServer struct {
	Upgrader *websocket.Upgrader
}

func InitWebSocketSvr() *WsServer {
	upgrader := &WsServer{Upgrader: &websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool { return true },
	}}
	return upgrader
}

func (ws *WsServer) Handle(name string, f func(conn *websocket.Conn) bool ) {
	http.HandleFunc(name, func(w http.ResponseWriter, r *http.Request) {
		c, err := ws.Upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println("upgrade:", err)
			return
		}
		defer func() {
			log.Println("disconnect !!")
			_ = c.Close()
		}()
		for {
			isBreak := f(c)
			if !isBreak {
				break
			}
		}
	})
}
