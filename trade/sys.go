package trade

import (
	"context"
	"docker/connect"
	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
	"log"
	"time"
)

type Trade struct {
	RedisClient *redis.Client
	RedisContext context.Context
	Remote string
	LastDataKey string
	WebSocketSvr *connect.WsServer
}

func InitTrade() *Trade{
	return &Trade{
		RedisClient: connect.InitRedisClient(),
		RedisContext: context.Background(),
		Remote: "wss://stream.yshyqxx.com/stream?streams=btcusdt@aggTrade",
		LastDataKey: "streams=btcusdt@aggTrade",
		WebSocketSvr: connect.InitWebSocketSvr(),
	}
}

func (trade *Trade) HandleWsApis() {
	trade.WebSocketSvr.Handle("/lastAggTrade", trade.ApisLastAggTrade)
	trade.WebSocketSvr.Handle("/aggTrade", trade.ApisAggTrade)
}

func (trade *Trade)StartReceiveAddTrade(){
	wsClient := connect.InitWebSocketClient(trade.Remote)

	wsClient.Client.OnConnected(func() {
		log.Println("OnConnected: ", wsClient.Client.WebSocket.Url)
		go func() {
			t := time.NewTicker(10 * time.Second)
			for {
				select {
				case <-t.C:
					_ = wsClient.Client.WebSocket.Conn.WriteMessage(websocket.PingMessage, []byte(""))
				}
			}
		}()
	})
	wsClient.Client.OnDisconnected(func(err error) {
		log.Println("OnDisconnected: ", err.Error())
	})
	wsClient.Client.OnClose(func(code int, text string) {
		log.Println("OnClose: ", code, text)
		wsClient.Done <- true
	})
	wsClient.Client.OnPingReceived(func(appData string) {
		log.Println("OnPingReceived")
	})
	wsClient.Client.OnPongReceived(func(appData string) {
		log.Println("OnPongReceived: ", appData)
	})
	wsClient.Client.OnTextMessageReceived(func(message string) {
		//log.Println("OnTextMessageReceived: ", message)
		_ = trade.SaveAddTrade(message)
	})
	go wsClient.Client.Connect()
	for {
		select {
		case <-wsClient.Done:
			return
		}
	}
}

func (trade *Trade) SaveAddTrade(tradeInfo string) bool {
	err := trade.RedisClient.Set(trade.RedisContext, trade.LastDataKey, tradeInfo, 0).Err()
	if err != nil {
		return false
	}
	return true
}

func (trade *Trade) GetLatestAddTrade() (*string, error) {
	tradeInfo, err := trade.RedisClient.Get(trade.RedisContext, trade.LastDataKey).Result()
	if err != nil {
		return nil, err
	}
	return &tradeInfo, nil
}