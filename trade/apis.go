package trade

import (
	"github.com/gorilla/websocket"
	"log"
)

func (trade *Trade) ApisLastAggTrade(c *websocket.Conn) bool {
	tradeInfo, err := trade.GetLatestAddTrade()
	if err != nil {
		log.Println(err)
	}
	err = c.WriteMessage(websocket.TextMessage, []byte(*tradeInfo))
	if err != nil {
		log.Println("write:", err)
	}
	return false
}

func (trade *Trade) ApisAggTrade(c *websocket.Conn) bool {
	tradeInfo, err := trade.GetLatestAddTrade()
	if err != nil {
		log.Println(err)
	}
	err = c.WriteMessage(websocket.TextMessage, []byte(*tradeInfo))
	if err != nil {
		log.Println("write:", err)
	}
	return true
}