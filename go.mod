module docker

go 1.14

require (
	github.com/go-redis/redis/v8 v8.11.5
	github.com/gorilla/websocket v1.5.0
	github.com/jpillora/backoff v1.0.0
	github.com/togettoyou/wsc v1.3.1 // indirect
)
